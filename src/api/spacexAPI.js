// Import axios
import axios from 'axios'
 

/*
    Instantiate a default SpaceX API with base URL
*/
const spacexAPI = axios.create({
    baseURL: 'https://api.spacexdata.com/v3'
});
 
export default spacexAPI;
