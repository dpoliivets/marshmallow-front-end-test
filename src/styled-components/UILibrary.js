// Import dependencies
import styled from 'styled-components'
import { Constants } from '../constants/Constants'

/*
    Page layout components
*/
export const PageContainer = styled.div`
    width: 100vw;
    height: auto;
`;
export const ContentLayout = styled.main`
    width: 100vw;
    padding-left: 12%;
    padding-right: 12%;
`;

/*
    Fluid text components
*/
export const HeaderTitleText = styled.h1`
    font-family: 'Orbitron', sans-serif;
    font-weight: 500;
    font-size: calc(${ Constants.minHeaderTitle }px + (${ Constants.maxHeaderTitle } - ${ Constants.minHeaderTitle }) * ((100vw - 300px) / (1600 - 300)));
    line-height: 1.5;
    letter-spacing: 6px;
    color: ${ Constants.headerFontColour };
    margin: 0 !important;
    text-shadow: 2px 2px 1px #0D1019;
`;
export const TitleText = styled.h1`
    font-family: 'Orbitron', sans-serif;
    font-weight: 500;
    font-size: calc(${ Constants.minTitle }px + (${ Constants.maxTitle } - ${ Constants.minTitle }) * ((100vw - 300px) / (1600 - 300)));
    line-height: 1.5;
    letter-spacing: 3px;
    color: ${ Constants.fontColour };
    margin: 0 !important;
    ${ props => props.colour ? 'color: '+props.colour : ''};
`;
export const SubTitleText = styled.h1`
    font-family: 'Orbitron', sans-serif;
    font-weight: 500;
    font-size: calc(${ Constants.minSubTitle }px + (${ Constants.maxSubTitle } - ${ Constants.minSubTitle }) * ((100vw - 300px) / (1600 - 300)));
    line-height: normal;
    letter-spacing: 3px;
    color: ${ Constants.fontColour };
    margin: 0 !important;
    ${ props => props.colour ? 'color: '+props.colour : ''};
`;
export const ParagraphText = styled.p`
    font-family: 'Orbitron', sans-serif;
    font-weight: 400;
    font-size: calc(${ Constants.minParagraphText }px + (${ Constants.maxParagraphText } - ${ Constants.minParagraphText }) * ((100vw - 300px) / (1600 - 300)));
    letter-spacing: 2px;
    line-height: 1.7;
    color: ${ Constants.fontColour };
    margin: 0 !important;
    ${ props => props.colour ? 'color: '+props.colour : ''};
    ${ props => props.bold ? 'font-weight: 500' : ''};
    ${ props => props.textAlign ? 'text-align: '+props.textAlign : ''};
    ${ props => props.fontSize ? 'font-size: '+props.fontSize : ''};
    ${ props => props.margin ? props.margin : ''};
    ${ props => props.underline ? 'text-decoration: underline' : ''};
`;
