// Import dependencies
import React from 'react'

// Import components
import { Tabs } from 'antd'
import TabPanel from './TabPanel'
import PopUp from '../popup/PopUp'
import { Spin } from 'antd'

// Import styles
import './TabBar.css'

// Import redux
import { connect } from 'react-redux'
import { getRocketData } from '../../redux/actions/rocketAction'
import { getDragonsData } from '../../redux/actions/dragonAction'

// Define tabs pane
const { TabPane } = Tabs;


/*
    Component that holds both dragons and rockets tabs.
    Calls a redux method that performs fetching.
*/
class TabBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            popUpActive: false,
            popUpData: {},
            rocketPopUp: true
        }
        this.openPopUp = this.openPopUp.bind(this);
        this.closePopUp = this.closePopUp.bind(this);
    }

    /*
        Opens pop-up for a currently selected rocket
    */
    openPopUp = (data, isRocket) => {                
        this.setState({ popUpActive: true, popUpData: data, rocketPopUp: isRocket });
    }

    /*
        Closes pop-up for a currently selected rocket
    */
    closePopUp = () => {
        this.setState({ popUpActive: false });
    }

    /*
        Fetch Rockets and Dragons data from SpaceX API
    */
    componentDidMount() {
        this.props.dispatch(getRocketData())
        this.props.dispatch(getDragonsData())
    }

    render() {
        // Get data
        const {
            rockets, rocketsLoading,
            dragons, dragonsLoading
        } = this.props;

        return (
            <div>
                <Tabs defaultActiveKey="1">
                    <TabPane tab="Rockets" key="1">
                        {
                            rocketsLoading ?
                                <div className='tabbar__loading-container'>
                                    <Spin size='large' />
                                </div>
                                :
                                <TabPanel data={rockets} isRocket={true} openPopUp={this.openPopUp} />
                        }
                    </TabPane>
                    <TabPane tab="Dragons" key="2">
                        {
                            dragonsLoading ?
                                <div className='tabbar__loading-container'>
                                    <Spin size='large' />
                                </div>
                                :
                                <TabPanel data={dragons} isRocket={false} openPopUp={this.openPopUp} />
                        }
                    </TabPane>
                </Tabs>

                {/* Pop-up window with extended information about the rocket */}
                <PopUp
                    active={this.state.popUpActive}
                    closePopUp={this.closePopUp}
                    data={this.state.popUpData}
                    isRocket={this.state.rocketPopUp}
                />
            </div>
        );
    }
}

// Connect redux to component
// const mapStateToProps = (state) => { return state }
const mapStateToProps = state => ({
    rockets: state.rockets.rocketData,
    rocketsLoading: state.rockets.isRocketDataLoading,
    rocketsFailed: state.rockets.rocketLoadingFailed,
    dragons: state.dragons.dragonData,
    dragonsLoading: state.dragons.isDragonDataLoading,
    dragonsFailed: state.dragons.dragonLoadingFailed
});
export default connect(mapStateToProps)(TabBar)
