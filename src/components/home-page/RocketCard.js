// Import dependencies
import React from 'react'

// Import components
import { Row, Col } from 'antd'
import { ParagraphText, TitleText, SubTitleText } from '../../styled-components/UILibrary'

// Import styles
import './RocketCard.css'


/*
    Rocket or Dragon card component
*/
const RocketCard = (props) => {
    return (
        <div className='rocket-card__container' onClick={() => props.openPopUp(props.data, props.isRocket)}>
            {/* Card Title */}
            <TitleText>{props.isRocket ? props.data.rocket_name : props.data.name}</TitleText>

            {/* Spaceship Info */}
            <Row className='rocket-card__info-row' type='flex'>
                <Col xs={24} sm={24} md={12} lg={24} xl={12} className='rocket-card__info-col'>
                    <SubTitleText>First Flight</SubTitleText>
                    <ParagraphText margin='margin-top: 8px !important'>{props.data.first_flight}</ParagraphText>
                </Col>
                <Col xs={24} sm={24} md={12} lg={24} xl={12} className='rocket-card__info-col'>
                    <SubTitleText>Status</SubTitleText>
                    <ParagraphText margin='margin-top: 8px !important'>{props.data.active ? 'ACTIVE' : 'NOT ACTIVE'}</ParagraphText>
                </Col>
                <Col xs={24} sm={24} md={12} lg={24} xl={12} className='rocket-card__info-col'>
                    <SubTitleText>Height</SubTitleText>
                    <ParagraphText margin='margin-top: 8px !important'>{props.isRocket ? props.data.height.meters + ' m' : props.data.height_w_trunk.meters + ' m'}</ParagraphText>
                </Col>
                <Col xs={24} sm={24} md={12} lg={24} xl={12} className='rocket-card__info-col'>
                    <SubTitleText>Diameter</SubTitleText>
                    <ParagraphText margin='margin-top: 8px !important'>{props.data.diameter.meters}</ParagraphText>
                </Col>
            </Row>

            {/* Spaceship Image */}
            {
                props.isRocket ?
                    <img
                        src={require('../../assets/images/objects/rocket-min.png')}
                        className='rocket-card__spaceship-image rocket-card__rocket' alt='rocket'
                    />
                    :
                    <img
                        src={require('../../assets/images/objects/dragon-min.png')}
                        className='rocket-card__spaceship-image rocket-card__dragon' alt='dragon'
                    />
            }
        </div>
    );
};

export default RocketCard;
