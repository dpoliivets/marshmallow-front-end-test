// Import dependencies
import React from 'react'

// Import components
import { Row, Col } from 'antd'
import RocketCard from './RocketCard'
import { ContentLayout } from '../../styled-components/UILibrary'


/*
    Individual tab panel component
*/
const TabPanel = (props) => {
    return (
        <ContentLayout>
            {/* Container for grid which displays spacecrafts */}
            <Row gutter={60} type='flex'>
                {
                    // Render fetched data
                    props.data.map((data) =>
                        <Col xs={24} sm={24} md={24} lg={12} xl={12} key={data.id}>
                            <RocketCard
                                data={data}
                                isRocket={props.isRocket}
                                openPopUp={props.openPopUp}
                            />
                        </Col>
                    )
                }
            </Row>
        </ContentLayout>
    );
};

export default TabPanel;

