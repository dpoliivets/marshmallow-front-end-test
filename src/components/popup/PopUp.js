// Import dependencies
import React from 'react'

// Import components
import { Row, Col } from 'antd'
import { ParagraphText, TitleText, SubTitleText } from '../../styled-components/UILibrary'

// Import styles
import './PopUp.css'

// Import images
import cross from '../../assets/svg/close.svg'


/*
    Pop-up panel component for displaying extra
    data about a spacecraft
*/
export default class PopUp extends React.Component {
    render() {
        return (
            <div
                className={this.props.active ? 'pop-up__container pop-up__active' : 'pop-up__container'}
            >
                {/* Close icon */}
                <div className='pop-up__close-icon' onClick={() => this.props.closePopUp()}>
                    <img src={cross} className='pop-up__cross' alt='cross' />
                </div>

                {/* Spacecraft name */}
                <TitleText>
                    {this.props.isRocket ? this.props.data.rocket_name : this.props.data.name}
                </TitleText>

                {/* Information container */}
                <Row type='flex' className='pop-up__info-container'>
                    {/* Description paragraph */}
                    <Col xs={20} sm={16} md={16} lg={12} xl={12}>
                        <div className='pop-up__description-container'>
                            <ParagraphText>{this.props.data.description}</ParagraphText>
                        </div>
                    </Col>
                    {/* Extra information */}
                    <Row type='flex' className='pop-up__extra-info-container'>
                        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                            <SubTitleText>
                                {
                                    this.props.isRocket ?
                                        'Cost Per Launch'
                                        :
                                        'Crew Capacity'
                                }
                            </SubTitleText>
                            <ParagraphText margin='margin-top: 8px !important'>
                                {
                                    this.props.isRocket ?
                                        '$ ' + this.props.data.cost_per_launch
                                        :
                                        this.props.data.crew_capacity
                                }
                            </ParagraphText>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                            <SubTitleText>
                                {
                                    this.props.isRocket ?
                                        'Success Rate'
                                        :
                                        'Dry Mass'
                                }
                            </SubTitleText>
                            <ParagraphText margin='margin-top: 8px !important'>
                                {
                                    this.props.isRocket ?
                                        this.props.data.success_rate_pct + '.00 %'
                                        :
                                        this.props.data.dry_mass_kg + ' kg'
                                }
                            </ParagraphText>
                        </Col>
                    </Row>
                </Row>

                <a href={this.props.data.wikipedia}>
                    <ParagraphText margin='margin-top: 10px !important' underline bold>
                        read more
                    </ParagraphText>
                </a>

                {/* Rocket image */}
                {
                    this.props.isRocket ?
                        <img
                            src={require('../../assets/images/objects/rocket-large-min.png')}
                            alt='spacecraft'
                            className='pop-up__spaceship-image pop-up__rocket'
                        />
                        :
                        <img
                            src={require('../../assets/images/objects/dragon-min.png')}
                            alt='spacecraft'
                            className='pop-up__spaceship-image pop-up__dragon'
                        />
                }
            </div>
        )
    }
}
