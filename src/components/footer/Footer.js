// Import dependencies
import React from 'react'

// Import components
import { ParagraphText } from '../../styled-components/UILibrary'

// Import styles
import './Footer.css'


/*
    Footer component
*/
const Footer = () => {
    return (
        <footer className='footer__container'>            
            <ParagraphText fontSize='10px'>
                by Dmitry Poliyivets for Marshmallow
            </ParagraphText>
        </footer>
    );
};

export default Footer;
