// Import dependencies
import React from 'react'

// Import components
import ScrollDown from './ScrollDown'
import { ParagraphText, HeaderTitleText } from '../../styled-components/UILibrary'

// Import styles
import './Header.css'

// Import images
import logo from '../../assets/svg/logo.svg'


/*
    Header component
*/
const Header = () => {
    return (
        <header className='header__container'>
            {/* Top Panel */}
            <div className='header__top-container'>
                <img src={logo} className='header-logo' alt='logo' />
                <div className='header__top-panel-info'>
                    <ParagraphText colour={'#fff'} textAlign='right'>
                        by Dmitry Poliyivets
                    </ParagraphText>
                    <ParagraphText colour={'#C1C1C1'} textAlign='right' fontSize={'10px'}>
                        for Marshmallow
                    </ParagraphText>
                </div>
            </div>

            {/* Title */}
            <div className='header__title'>
                <HeaderTitleText>
                    Explore SpaceX<br/>
                    Rockets &<br/>
                    Dragons
                </HeaderTitleText>
            </div>

            {/* Scroll Down Component */}
            <ScrollDown />
        </header>
    );
};

export default Header;
