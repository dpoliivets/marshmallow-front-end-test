// Import dependencies
import React from 'react'

// Import styles
import './ScrollDown.css'

// Import images
import arrow from '../../assets/svg/down-arrow.svg'


/*
    Scroll down indicator
*/
const ScrollDown = () => {
    return (
        <div className='scroll-down__container'>
            <img src={arrow} className='scroll-down__arrow' alt='down-arrow'/>
            <img src={arrow} className='scroll-down__arrow' alt='down-arrow'/>
            <img src={arrow} className='scroll-down__arrow' alt='down-arrow'/>
        </div>
    );
};

export default ScrollDown;
