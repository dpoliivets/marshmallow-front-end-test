/*
    This file contains constants used
    throughout the web app
*/
export const Constants = {
    // Font Colours
    fontColour: '#0D1019',
    headerFontColour: '#fff',
    // Font Sizes
    minParagraphText: 12,
    maxParagraphText: 14,
    minHeaderTitle: 30,
    maxHeaderTitle: 56,
    minTitle: 16,
    maxTitle: 22,
    minSubTitle: 13,
    maxSubTitle: 15,
};