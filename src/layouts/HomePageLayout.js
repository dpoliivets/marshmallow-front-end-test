// Import dependencies
import React from 'react'

// Import components
import Header from '../components/header/Header'
import TabBar from '../components/home-page/TabBar'
import Footer from '../components/footer/Footer'
import { PageContainer } from '../styled-components/UILibrary'


export default class HomePageLayout extends React.Component {
    render() {
        return (
            <PageContainer>
                {/* Header */}
                <Header />

                {/* Page Content */}
                <TabBar />

                {/* Footer */}
                <Footer />
            </PageContainer>
        );
    }
}
