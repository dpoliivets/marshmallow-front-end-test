// Import API
import spacexAPI from '../../api/spacexAPI';

/*
    Define action types for Rocket fetching
*/
export const FETCH_ROCKETS_BEGIN = 'FETCH_ROCKETS_BEGIN'
export const FETCH_ROCKETS_SUCCESS = 'FETCH_ROCKETS_SUCCESS'
export const FETCH_ROCKETS_FAILURE = 'FETCH_ROCKETS_FAILURE'


/*
    Actions for pulling data from SpaseX Rocket API
*/
export function getRocketData() {
    return (dispatch) => {
        // Dipatch begin event
        dispatch(fetchRocketsBegin());
        spacexAPI.get(
            '/rockets/',
            {
                params: {
                    // Filter results to get only what I need
                    filter: 'id,active,cost_per_launch,success_rate_pct,first_flight,height/meters,diameter/meters,wikipedia,description,rocket_name'
                }
            })
            .then(response => {
                dispatch(fetchRocketsSuccess(response.data));
            })
            .catch(error => {
                dispatch(fetchRocketsFailure());
            })
    }
}
export const fetchRocketsBegin = () => ({
    type: FETCH_ROCKETS_BEGIN
});
export const fetchRocketsSuccess = (data) => ({
    type: FETCH_ROCKETS_SUCCESS,
    payload: data
});
export const fetchRocketsFailure = () => ({
    type: FETCH_ROCKETS_FAILURE
});
