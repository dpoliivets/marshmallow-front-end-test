// Import API
import spacexAPI from '../../api/spacexAPI';

/*
    Define action types for Dragons fetching
*/
export const FETCH_DRAGONS_BEGIN = 'FETCH_DRAGONS_BEGIN'
export const FETCH_DRAGONS_SUCCESS = 'FETCH_DRAGONS_SUCCESS'
export const FETCH_DRAGONS_FAILURE = 'FETCH_DRAGONS_FAILURE'


/*
    Actions for pulling data from SpaseX Dragons API
*/
export function getDragonsData() {
    return (dispatch) => {
        // Dipatch begin event
        dispatch(fetchDragonsBegin());
        spacexAPI.get(
            '/dragons/',
            {
                params: {
                    // Filter results to get only what I need
                    filter: 'id,active,name,crew_capacity,first_flight,diameter/meters,wikipedia,description,height_w_trunk/meters,dry_mass_kg'
                }
            })
            .then(response => {
                dispatch(fetchDragonsSuccess(response.data));
            })
            .catch(error => {
                dispatch(fetchDragonsFailure());
            })
    }
}
export const fetchDragonsBegin = () => ({
    type: FETCH_DRAGONS_BEGIN
});
export const fetchDragonsSuccess = (data) => ({
    type: FETCH_DRAGONS_SUCCESS,
    payload: data
});
export const fetchDragonsFailure = () => ({
    type: FETCH_DRAGONS_FAILURE
});