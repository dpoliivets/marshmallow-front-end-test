/*
    Import action types for Rockets fetching
*/
import { FETCH_ROCKETS_BEGIN, FETCH_ROCKETS_SUCCESS, FETCH_ROCKETS_FAILURE } from '../actions/rocketAction'

/*
    Define default state
*/
let defaultState = {
    isRocketDataLoading: true,
    rocketLoadingFailed: false
}

/*
    Define rocket reducer
*/
const rocketReducer = (state = defaultState, action) => {
    switch (action.type) {
        case FETCH_ROCKETS_BEGIN:
            return {
                isRocketDataLoading: true
            };
        case FETCH_ROCKETS_SUCCESS:
            return {
                isRocketDataLoading: false,
                rocketData: action.payload
            };
        case FETCH_ROCKETS_FAILURE:
            return {
                isRocketDataLoading: true,
                rocketLoadingFailed: true
            };
        default: return state;
    }
}

export default rocketReducer;