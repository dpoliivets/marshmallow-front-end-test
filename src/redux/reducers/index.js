import { combineReducers } from 'redux'
import rockets from './rocketReducer'
import dragons from './dragonReducer';

export default combineReducers({
    rockets,
    dragons
});
