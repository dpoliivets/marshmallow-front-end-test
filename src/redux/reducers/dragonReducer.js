/*
    Import action types for Rockets fetching
*/
import { FETCH_DRAGONS_BEGIN, FETCH_DRAGONS_SUCCESS, FETCH_DRAGONS_FAILURE } from '../actions/dragonAction'

/*
    Define default state
*/
let defaultState = {
    isDragonDataLoading: true,
    dragonLoadingFailed: false
}

/*
    Define Dragon reducer
*/
const dragonReducer = (state = defaultState, action) => {
    switch (action.type) {
        case FETCH_DRAGONS_BEGIN:
            return {
                isDragonDataLoading: true
            };
        case FETCH_DRAGONS_SUCCESS:
            return {
                isDragonDataLoading: false,
                dragonData: action.payload
            };
        case FETCH_DRAGONS_FAILURE:
            return {
                isDragonDataLoading: true,
                dragonLoadingFailed: true
            };
        default: return state;
    }
}

export default dragonReducer;