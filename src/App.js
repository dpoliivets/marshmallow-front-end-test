// Import dependencies
import React from 'react';

// Import styles
import './App.css';

// Import layout
import HomePageLayout from './layouts/HomePageLayout';

function App() {
  return (
    <HomePageLayout />
  );
}

export default App;
