# Marshmallow Front-End Test
by Dmytro Poliyivets.


### Quick Note to Test Creator
> Thank you very much for choosing an interesting objective for the test!
> To be honest, I never thought that I will say something like this, but 
> I really enjoyed doing the test! I wish all tests were like this.

### Preview
If you don't want to download the repo and install the dependencies, you can preview the result on Netlify [marshmallow-test.netlify.com](https://marshmallow-test.netlify.com/)

### Installation

You’ll need to have Node 8.16.0 or Node 10.16.0 or later version on your local machine to run this code.

Install the dependencies and start the server.

```sh
$ cd marshmallow-front-end-test
$ npm install
$ npm start
```

### Tech

* [ReactJS]
* [styled-components]
* [ant.design]
* [Redux]
* [redux-thunk]
* [Fluid typography]

### Resources

* [Flat Icon] – to minimise size compared to importing all icons from Ant.design
* [Unsplash]
* [Google Fonts]

[ReactJS]: <https://reactjs.org/>
[styled-components]: <https://www.styled-components.com/>
[ant.design]: <https://ant.design/>
[Redux]: <https://redux.js.org/introduction/getting-started/>
[redux-thunk]: <https://github.com/reduxjs/redux-thunk/>
[Fluid typography]: <https://css-tricks.com/snippets/css/fluid-typography/>
[Flat Icon]: <https://www.flaticon.com/home>
[Unsplash]: <https://unsplash.com/>
[Google Fonts]: <https://fonts.google.com/>